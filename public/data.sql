-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para crud_ci3
CREATE DATABASE IF NOT EXISTS `crud_ci3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crud_ci3`;

-- Volcando estructura para tabla crud_ci3.perfil
CREATE TABLE IF NOT EXISTS `perfil` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `per_nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`per_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla crud_ci3.perfil: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT IGNORE INTO `perfil` (`per_id`, `per_nombre`) VALUES
	(1, 'ADMINISTRADOR'),
	(2, 'VENDEDOR');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;

-- Volcando estructura para tabla crud_ci3.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `per_id` int(11) NOT NULL,
  `usu_nombres` varchar(50) DEFAULT NULL,
  `usu_apellidos` varchar(100) DEFAULT NULL,
  `usu_correo` varchar(50) DEFAULT NULL,
  `usu_telefono` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`usu_id`),
  KEY `fk_usuario_perfil_idx` (`per_id`),
  CONSTRAINT `fk_usuario_perfil` FOREIGN KEY (`per_id`) REFERENCES `perfil` (`per_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla crud_ci3.usuario: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT IGNORE INTO `usuario` (`usu_id`, `per_id`, `usu_nombres`, `usu_apellidos`, `usu_correo`, `usu_telefono`) VALUES
	(1, 1, 'Yeffer', 'Buitrago', 'asd@asd.com', '101010101'),
	(3, 2, 'asdasd', 'asdasd', 'asdasd', 'asd'),
	(4, 1, 'carlos A', 'cccc', 'asd@asd.com', '301320200');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
