<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CRUD CODEIGNITER</title>
        <link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.css') ?>"> 
        <script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('public/js/bootstrap.min.js') ?>"></script>  
    </head>
    <body>     
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">                            
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">CRUD - CODEIGNITER</a>
                            </div>                            
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="<?php echo base_url('') ?>">INICIO <span class="sr-only">(current)</span></a></li>                                    
                                </ul>                                                           
                            </div>
                        </div>
                    </nav>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $this->load->view($contenido);
                            ?>
                        </div>                      
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>       