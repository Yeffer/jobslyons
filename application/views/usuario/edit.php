<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <form action="<?php echo base_url('usuario/update') ?>" method="POST" >         
            <?php foreach ($datosUsuario as $value) { ?>

                <input class="form-control" type="hidden"  name="txtUsuid" value="<?php echo $value->usu_id; ?>" >

                <div class="form-group">        
                    <label for="name" class="col-sm-2 control-label">Perfil:</label>
                    <div class="col-sm-10">

                        <?php
                        foreach ($selPerfil as $registro) {

                            $lista[$registro->per_id] = $registro->per_nombre;
                        }

                        echo form_dropdown('txtPerid', $lista, $value->per_id, 'class="form-control"')
                        ?>            

                    </div>
                </div>  
                <div class="form-group">        
                    <label for="name" class="col-sm-2 control-label">Nombre:</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text"  name="txtNombre" value="<?php echo $value->usu_nombres; ?>" >
                    </div>
                </div>  
                <div class="form-group">        
                    <label for="name" class="col-sm-2 control-label">Apellido:</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text"  name="txtApellido" value="<?php echo $value->usu_apellidos; ?>" >
                    </div>
                </div>  
                <div class="form-group">        
                    <label for="name" class="col-sm-2 control-label">Email:</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="email"  name="txtEmail" value="<?php echo $value->usu_correo; ?>" >
                    </div>
                </div>  
                <div class="form-group">        
                    <label for="name" class="col-sm-2 control-label">Telefono:</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text"  name="txtTelefono" value="<?php echo $value->usu_telefono; ?>" >
                    </div>
                </div>  
                <div class="form-group">                        
                    <div class="col-lg-8">
                    <?php } ?> 
                    <input class="btn btn-success" type="submit" name="accion" id="crear" value="Actualizar" />
                </div>
            </div>                                            
        </form>
    </div>
</div>


