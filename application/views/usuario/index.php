<div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#consulta" aria-controls="consulta" role="tab" data-toggle="tab">Consulta</a></li>
        <li role="presentation"><a href="#registro" aria-controls="registro" role="tab" data-toggle="tab">Registro</a></li>    
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="consulta">
            <div class="container">  
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Perfil</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Telefoo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listaUsuario as $value) { ?>
                            <tr>
                                <td><?php echo $value->usu_id; ?></td>
                                <td><?php echo $value->per_nombre; ?></td>
                                <td><?php echo $value->usu_nombres; ?></td>
                                <td><?php echo $value->usu_apellidos; ?></td>
                                <td><?php echo $value->usu_correo; ?></td>
                                <td><?php echo $value->usu_telefono; ?></td>
                                <td>
                                    <a href="<?php echo base_url('usuario/delete/') . $value->usu_id; ?>" class='btn btn-danger glyphicon glyphicon-trash  ' ></a>
                                    <a href="<?php echo base_url('usuario/edit/') . $value->usu_id; ?>"class='btn btn-info glyphicon glyphicon-edit'></a></td>                             
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="registro">
            <div class="row">
                <div class="col-md-8">
                    <form action="<?php echo base_url('usuario/insert') ?>" method="POST" >                
                        <div class="form-group">        
                            <label for="name" class="col-sm-2 control-label">Perfil:</label>
                            <div class="col-sm-10">
                                <select name="txtIdper" class="form-control">
                                    <option></option>
                                    <?php foreach ($selPerfil as $value) { ?>
                                        <option value="<?php echo $value->per_id ?>"> <?php echo $value->per_nombre ?></option>
                                    <?php } ?>            
                                </select>          
                            </div>
                        </div>  
                        <div class="form-group">        
                            <label for="name" class="col-sm-2 control-label">Nombre:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text"  name="txtNombre" value="" placeholder="Nombre" required data-msg="Nombre Requerido" >
                            </div>
                        </div>  
                        <div class="form-group">        
                            <label for="name" class="col-sm-2 control-label">Apellido:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text"  name="txtApellido" value="" placeholder="Apellido" required data-msg="Apellido Requerido" >
                            </div>
                        </div>  
                        <div class="form-group">        
                            <label for="name" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="email"  name="txtEmail" value="" placeholder="Email" required data-msg="Email Requerido" >
                            </div>
                        </div>  
                        <div class="form-group">        
                            <label for="name" class="col-sm-2 control-label">Telefono:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text"  name="txtTelefono" value="" placeholder="Telefono" required data-msg="Telefono Requerido" >
                            </div>
                        </div>  
                        <div class="form-group">                        
                            <div class="col-lg-8">
                                <input class="btn btn-success" type="submit" name="accion" id="crear" value="Guardar" />
                            </div>
                        </div>                                            
                    </form>   
                </div>
            </div>
        </div>    
    </div>
</div>



