<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuario extends  CI_Controller 
{
 
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Model_Usuario');
    }
    
    public function index()
    {        
        $data['contenido'] = "usuario/index";
        $data['selPerfil'] = $this->Model_Usuario->selPerfil();
        $data['listaUsuario'] = $this->Model_Usuario->listUsuario();
        $this->load->view("plantilla", $data);
    }
    
    public function insert()
    {
        $datos = $this->input->post();
        
        if(isset($datos)){
            
            $txtId = $datos['txtIdper'];
            $txtNombre = $datos['txtNombre'];
            $txtApellido = $datos['txtApellido'];
            $txtEmail = $datos['txtEmail'];
            $txtTelefono = $datos['txtTelefono'];
            
            $this->Model_Usuario->insertUsuario($txtId, $txtNombre, $txtApellido, $txtEmail, $txtTelefono);
            redirect('');
     
        }
    }
    
    public function delete($id = NULL)
    {
        if($id != NULL)
        {
            $this->Model_Usuario->deleteUsuario($id);
            redirect('');
        }
    }
    
    public function edit($id = NULL)
    {
        if($id != NULL)
        {
            $data['contenido'] = 'usuario/edit';
            $data['selPerfil'] = $this->Model_Usuario->selPerfil();
            $data['datosUsuario'] = $this->Model_Usuario->editUsuario($id);
            
            $this->load->view('plantilla', $data);
            
        }else
        {
            redirect('');
        }
    }
    
    public function update()
    {
        $datos = $this->input->post();
        
        if(isset($datos)){
            
            $txtUsuId = $datos['txtUsuid'];
            $txtPerId = $datos['txtPerid'];
            $txtNombre = $datos['txtNombre'];
            $txtApellido = $datos['txtApellido'];
            $txtEmail = $datos['txtEmail'];
            $txtTelefono = $datos['txtTelefono'];
            
            $this->Model_Usuario->updateUsuario($txtUsuId,$txtPerId, $txtNombre, $txtApellido, $txtEmail, $txtTelefono);
            redirect('');
     
        }
    }
}