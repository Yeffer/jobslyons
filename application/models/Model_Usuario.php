<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Usuario extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function selPerfil()
    {
        $query = $this->db->query("SELECT * FROM perfil");
        return $query->result();
    }
    
    public function insertUsuario($idPer, $nombre, $apellido, $email, $telefono )
    {
        $arrayDatos = array(
            'per_id' => $idPer,
            'usu_nombres' => $nombre,
            'usu_apellidos' => $apellido,
            'usu_correo' => $email,
            'usu_telefono' => $telefono
        );
        
        $this->db->insert('usuario', $arrayDatos);
    
    }
    
    public function listUsuario()
    {
        $query = $this->db->query("SELECT * FROM usuario u INNER JOIN perfil p on u.per_id=p.per_id");
        return $query->result();
    }
    
    public function deleteUsuario($id)
    {
        $this->db->where('usu_id', $id);
        $this->db->delete('usuario');
    }
    
    public function editUsuario($id)
    {
        $query = $this->db->query("SELECT * FROM usuario u INNER JOIN perfil p on u.per_id = p.per_id WHERE u.usu_id = $id");
        return $query->result();
    }
    
    public function updateUsuario($txtUsuId,$txtPerId, $txtNombre, $txtApellido, $txtEmail, $txtTelefono)
    {
        $arrayData = array(
            'per_id' => $txtPerId,
            'usu_nombres' => $txtNombre,
            'usu_apellidos' => $txtApellido,
            'usu_correo' => $txtEmail,
            'usu_telefono' => $txtTelefono
        );
        
        $this->db->where('usu_id', $txtUsuId);
        $this->db->update('usuario',$arrayData);
        
        redirect('');
    }
}